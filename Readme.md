# Metabox

This is basically an abstraction layer around [Advanced Custom Fields](https://github.com/AdvancedCustomFields/acf).  
It's meant to complement [PostTypes](https://github.com/jjgrainger/PostTypes)!

## Requirements

- PHP >= 7.4
- [Composer](https://getcomposer.org/)
- most recent version of [WordPress](https://wordpress.org)
- [Advanced Custom Fields WordPress Plugin](https://wordpress.org/plugins/advanced-custom-fields/) installed and active

## Installation

```
composer require dweipert/metaboxes
```

## How to

```php
use PostTypes\Metabox;
use PostTypes\MetaboxField;

// create a metabox
$metabox = new Metabox('Meta');

// assign a post type to it
$metabox->posttype('post');

// create a field for the box
$field = new MetaboxField('image');

// set the fields ACF type
$field->type('image');

// add the field to the metabox
$metabox->field($field);

// add the box and its fields with ACF
$metabox->add();
```
