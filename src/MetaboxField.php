<?php

namespace PostTypes;

class MetaboxField
{
    /**
     * The field's key
     */
    public string $key;

    /**
     * The field's names
     * 'label' is the title
     * 'name' is the meta_key
     * The $key is derived from the label
     */
    public array $names;

    /**
     * The field's type
     * Can be one of the following
     * @see https://www.advancedcustomfields.com/resources/register-fields-via-php/#field-type%20settings
     */
    public string $type;

    /**
     * The Metabox's $key
     */
    public string $parent;

    /**
     * The "ACF Field Settings"
     * @see https://www.advancedcustomfields.com/resources/register-fields-via-php/#field-settings
     * There are extra settings available based on the field's type
     * @see https://www.advancedcustomfields.com/resources/register-fields-via-php/#field-type%20settings
     */
    public array $options = [];

    /**
     * MetaboxField constructor.
     *
     * @param string|array $names
     * @param array        $options
     */
    public function __construct($names, array $options = [])
    {
        // only the name is passed
        if (is_string($names)) {
            $names = [
                // create a human friendly name
                'label' => ucwords(strtolower(str_replace(['-', '_'], ' ', $names))),
                'name' => $names,
            ];
        }

        // @see https://github.com/AdvancedCustomFields/acf/blob/5.9.3/includes/local-fields.php#L365
        $this->key = 'field_' . $names['name'];
        $this->names = $names;

        $this->options($options);
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function type(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param string $parent
     *
     * @return $this
     */
    public function parent(string $parent)
    {
        if (strpos($parent, 'group') !== 0) {
            $parent = "group_$parent";
        }

        $this->parent = $parent;

        return $this;
    }

    /**
     * @param array $options
     *
     * @return $this
     */
    public function options(array $options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @param float $width
     *
     * @return $this
     */
    public function width(float $width) {
        $this->options['wrapper'] ??= [];
        $this->options['wrapper']['width'] = $width < 1 ? $width*100 : $width;

        return $this;
    }

    /**
     * @param string $class
     *
     * @return $this
     */
    public function class(string $class) {
        $this->options['wrapper'] ??= [];
        $this->options['wrapper']['class'] = $class;

        return $this;
    }

    /**
     * Add the acf local field
     */
    public function add()
    {
        // merge with extra options
        $field = array_replace_recursive([
            'key' => $this->key,
            'label' => $this->names['label'],
            'name' => $this->names['name'],
            'type' => $this->type,
            'parent' => $this->parent,
        ], $this->options);

        // add the field
        acf_add_local_field($field);
    }
}
