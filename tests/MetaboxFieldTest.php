<?php

use PHPUnit\Framework\TestCase;
use PostTypes\MetaboxField;

class MetaboxFieldTest extends TestCase
{
    /**
     * @var MetaboxField
     */
    public $field;

    /**
     * @inheritDoc
     */
    protected function setUp(): void
    {
        $this->field = new MetaboxField('test_field');
    }

    /**
     * @test
     */
    public function hasFieldKeyOnInstantiation()
    {
        $this->assertEquals('field_test_field', $this->field->key);
    }

    /**
     * @test
     */
    public function hasNamesOnInstantiation()
    {
        $expectedNames = [
            'label' => 'Test Field',
            'name' => 'test_field',
        ];

        $this->assertEquals($expectedNames, $this->field->names);
    }

    /**
     * @test
     */
    public function adjustsParentGroupKey()
    {
        $this->field->parent('metabox_test');
        $this->assertEquals('group_metabox_test', $this->field->parent);

        $this->field->parent('group_metabox_test');
        $this->assertEquals('group_metabox_test', $this->field->parent);
    }
}
