<?php

use PHPUnit\Framework\TestCase;
use PostTypes\Metabox;
use PostTypes\MetaboxField;

class MetaboxTest extends TestCase
{
    /**
     * @var Metabox
     */
    public $metabox;

    /**
     * @var MetaboxField
     */
    public $field;

    /**
     * @inheritDoc
     */
    protected function setUp(): void
    {
        $this->metabox = new Metabox('Test Metabox');
        $this->field = new MetaboxField('test_field');
    }

    /**
     * @test
     */
    public function hasGroupKeyOnInstantiation()
    {
        $this->assertEquals('group_test_metabox', $this->metabox->key);
    }

    /**
     * @test
     */
    public function assignsPostType()
    {
        $this->metabox->posttype(['post', 'page']);

        $expectedLocations = [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'post'
            ],
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'page'
            ],
        ];

        $this->assertEquals($expectedLocations, $this->metabox->locations);
    }

    /**
     * @test
     */
    public function assignsTaxonomy()
    {
        $this->metabox->taxonomy(['category', 'post_tag']);

        $expectedLocations = [
            [
                'param' => 'taxonomy',
                'operator' => '==',
                'value' => 'category'
            ],
            [
                'param' => 'taxonomy',
                'operator' => '==',
                'value' => 'post_tag'
            ],
        ];

        $this->assertEquals($expectedLocations, $this->metabox->locations);
    }

    /**
     * @test
     */
    public function modifiesAndHoldsPassedField()
    {
        $this->metabox->field($this->field);

        $this->assertEquals($this->field->parent, $this->metabox->key);
        $this->assertEquals($this->metabox->fields[$this->field->key], $this->field);
    }

    /**
     * @test
     */
    public function addsMultipleFieldsAsArray()
    {
        $fields = [
            $this->field,
            $field2 = (new MetaboxField('field_2')),
        ];
        $this->metabox->field($fields);

        $expected = [
            $this->field->key => $this->field,
            $field2->key => $field2,
        ];
        $this->assertEquals($expected, $this->metabox->fields);
    }
}
